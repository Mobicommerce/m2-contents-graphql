<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_ContentsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\ContentsGraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Mobicommerce\Mobiapp\Model\SectionRepository;
use Mobicommerce\Mobiapp\Model\WidgetRepository;

class Sections implements ResolverInterface
{
    public function __construct(
        SectionRepository $sectionRepository,
        WidgetRepository $widgetRepository,
        Widgets\Inputs $inputs
    ) {
        $this->sectionRepository = $sectionRepository;
        $this->widgetRepository = $widgetRepository;
        $this->inputs = $inputs;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args['ids'])) {
            throw new GraphQlInputException(__('ids should be specified'));
        }

        $data = [];

        try {
            $this->inputs->setInfo($info);
            $this->inputs->setContext($context);
            
            $ids = $args['ids'];
            $data = $this->sectionRepository->getByIds($ids);

            foreach ($data as &$section) {
                $section['widgets'] = $this->widgetRepository->getWidgetsBySectionId($section['id']);
            }
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $data;
    }
}
