<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_ContentsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\ContentsGraphQl\Model\Resolver\PageBuilder;

use Magento\Framework\GraphQl\Query\Resolver\IdentityInterface;
use Mobicommerce\Mobiapp\Model\PageBuilder;
use Mobicommerce\Mobiapp\Model\Section;
use Mobicommerce\Mobiapp\Model\Widget;
use Mobicommerce\Mobiapp\Model\Image;
use Magento\Catalog\Model\Category;
use Mobicommerce\ContentsGraphQl\Model\Resolver\Sections\Identity as SectionsIdentity;

/**
 * Identity for resolved PageBuilder
 */
class Identity implements IdentityInterface
{
    public function __construct(
        SectionsIdentity $sectionsIdentity
    ) {
        $this->sectionsIdentity = $sectionsIdentity;
    }

    /**
     * Get PageBuilder ID from resolved data
     *
     * @param array $resolvedData
     * @return string[]
     */
    public function getIdentities(array $resolvedData): array
    {
        $ids = [];

        $pageBuilderInfo = $resolvedData['mobiPageBuilder'] ?? [];

        if ($pageBuilderInfo['id']) {
            $ids[] = sprintf('%s_%s', PageBuilder::CACHE_TAG, $pageBuilderInfo['id']);

            $leftpanelLinks = $pageBuilderInfo['leftpanel_links'] ?? [];
            if ($leftpanelLinks) {
                foreach ($leftpanelLinks as $linkWidget) {
                    $ids[] = sprintf('%s_%s', Widget::CACHE_TAG, $linkWidget['id']);
                }
            }
            
            $sections = $pageBuilderInfo['sections'] ?? [];
            if ($sections) {
                $_ids = $this->sectionsIdentity->getIdentities($sections);
                $ids = array_merge($ids, $_ids);
            }
        }

        $ids = array_unique($ids);

        return $ids;
    }
}
