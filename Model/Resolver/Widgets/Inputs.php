<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_ContentsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\ContentsGraphQl\Model\Resolver\Widgets;

class Inputs
{
    private $info;
    private $context;
    private $cmsPageId = null;
    private $productCollectionId = null;

    public function setInfo($info)
    {
        $this->info = $info;
    }

    public function getInfo()
    {
        return $this->info;
    }

    public function setContext($context)
    {
        $this->context = $context;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function setProductCollectionId($id)
    {
        $this->productCollectionId = $id;
    }

    public function getProductCollectionId()
    {
        return $this->productCollectionId;
    }

    public function setCmsPageId($id)
    {
        $this->cmsPageId = $id;
    }

    public function getCmsPageId()
    {
        return $this->cmsPageId;
    }
}
