<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_ContentsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\ContentsGraphQl\Model\Resolver\Widgets;

use Magento\Framework\GraphQl\Query\Resolver\IdentityInterface;
use Mobicommerce\Mobiapp\Model\Widget;
use Mobicommerce\Mobiapp\Model\Image;
use Magento\Cms\Model\Page;
use Mobicommerce\Mobiapp\Model\Productcollection;

/**
 * Identity for resolved PageBuilder
 */
class Identity implements IdentityInterface
{
    /**
     * Get PageBuilder ID from resolved data
     *
     * @param array $resolvedData
     * @return string[]
     */
    public function getIdentities(array $resolvedData): array
    {
        //$widgets = $resolvedData['mobiWidgets'] ?? [];
        $widgets = $resolvedData;

        if ($widgets) {
            foreach ($widgets as $widget) {
                $ids[] = sprintf('%s_%s', Widget::CACHE_TAG, $widget['id']);

                $banners = $widget['banners'] ?? [];
                if ($banners) {
                    foreach ($banners as $banner) {
                        $ids[] = sprintf('%s_%s', Image::CACHE_TAG, $banner['id']);
                    }
                }

                $productCmsPage = $widget['product_cms_page'] ?? [];
                if ($productCmsPage) {
                    $ids[] = sprintf('%s_%s', Page::CACHE_TAG, $productCmsPage['id']);

                    $ids[] = sprintf('%s_%s', Productcollection::CACHE_TAG, $productCmsPage['product_collection_id']);
                }
            }
        }

        $ids = array_unique($ids);

        return $ids;
    }
}
