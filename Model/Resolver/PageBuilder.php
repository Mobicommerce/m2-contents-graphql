<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_ContentsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\ContentsGraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Mobicommerce\Mobiapp\Model\PageBuilderRepository;
use Mobicommerce\Mobiapp\Model\SectionRepository;
use Mobicommerce\Mobiapp\Model\WidgetRepository;

class PageBuilder implements ResolverInterface
{
    public function __construct(
        PageBuilderRepository $pageBuilderRepository,
        SectionRepository $sectionRepository,
        WidgetRepository $widgetRepository,
        Widgets\Inputs $inputs
    ) {
        $this->pageBuilderRepository = $pageBuilderRepository;
        $this->sectionRepository = $sectionRepository;
        $this->widgetRepository = $widgetRepository;
        $this->inputs = $inputs;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args['id']) && !isset($args['homepage'])
            && !isset($args['mvHomePage'])
            && !isset($args['cmsPageId'])) {
            throw new GraphQlInputException(__('id or homepage should be specified'));
        }

        $data = [];

        try {
            $pageBuilderId = null;

            if (isset($args['id'])) {
                $pageBuilderId = (int)$args['id'];
            } elseif (isset($args['homepage'])) {
                $appType = $args['appType'] ?? "pwa";
                if($args['appType']=="mobileapps"){
                    $appType = "mobileapp";
                }
                $pageBuilderId = $this->pageBuilderRepository->getIdFromHomepageConfiguration($appType);
            } elseif (isset($args['cmsPageId']) || isset($args['mvHomePage'])) {
                $appType = $args['appType'] ?? "pwa";
                $cmsPageId = $args['cmsPageId'];
                $pageBuilderId = $this->pageBuilderRepository->getIdFromCmsId($appType);
            }

            if (!$pageBuilderId) {
                throw new GraphQlInputException(__('there is some issue configuring page builder.'));
            }

            $this->inputs->setInfo($info);
            $this->inputs->setContext($context);

            $data = $this->pageBuilderRepository->getById($pageBuilderId);
            $data['leftpanel_category_ids'] = $this->pageBuilderRepository->getLeftPanelCategoryIds($pageBuilderId);
            $data['leftpanel_links'] = $this->widgetRepository->getLinkBuilderWidgetsByPageBuilderId($pageBuilderId);

            $sections = $this->sectionRepository->getSectionsByPageBuilderId($pageBuilderId);
            if ($sections) {
                foreach ($sections as &$section) {
                    $section['widgets'] = $this->widgetRepository->getWidgetsBySectionId($section['id']);
                }
            }
            $data['sections'] = $sections;
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $data;
    }
}
