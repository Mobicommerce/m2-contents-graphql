<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_ContentsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\ContentsGraphQl\Model\Resolver\Sections;

use Magento\Framework\GraphQl\Query\Resolver\IdentityInterface;
use Mobicommerce\Mobiapp\Model\Section;
use Mobicommerce\Mobiapp\Model\Widget;
use Mobicommerce\Mobiapp\Model\Image;
use Mobicommerce\ContentsGraphQl\Model\Resolver\Widgets\Identity as WidgetsIdentity;

/**
 * Identity for resolved PageBuilder
 */
class Identity implements IdentityInterface
{
    public function __construct(
        WidgetsIdentity $widgetsIdentity
    ) {
        $this->widgetsIdentity = $widgetsIdentity;
    }

    /**
     * Get PageBuilder ID from resolved data
     *
     * @param array $resolvedData
     * @return string[]
     */
    public function getIdentities(array $resolvedData): array
    {
        $ids = [];

        //$sections = $resolvedData['mobiSections'] ?? [];
        $sections = $resolvedData;

        if ($sections) {
            if ($sections) {
                foreach ($sections as $section) {
                    $ids[] = sprintf('%s_%s', Section::CACHE_TAG, $section['id']);

                    $widgets = $section['widgets'] ?? [];

                    if ($widgets) {
                        $_ids = $this->widgetsIdentity->getIdentities($widgets);
                        $ids = $this->mergeArray($ids, $_ids);
                    }
                }
            }
        }

        $ids = array_unique($ids);

        return $ids;
    }

    public function mergeArray($arr1, $arr2)
    {
        return array_merge($arr1, $arr2);
    }
}
