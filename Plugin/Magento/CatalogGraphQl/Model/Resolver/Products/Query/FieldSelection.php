<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_ContentsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\ContentsGraphQl\Plugin\Magento\CatalogGraphQl\Model\Resolver\Products\Query;

use Magento\Framework\GraphQl\Query\FieldTranslator;

class FieldSelection
{
    /**
     * @var FieldTranslator
     */
    private $fieldTranslator;

    /**
     * @param FieldTranslator $fieldTranslator
     */
    public function __construct(FieldTranslator $fieldTranslator)
    {
        $this->fieldTranslator = $fieldTranslator;
    }

    public function aroundGetProductsFieldSelection($subject, $callable, $resolveInfo): array
    {
        $productFields = $resolveInfo->getFieldSelection(3);
        $productFields = $productFields['product_cms_page']['products'] ?? [];

        if (!$productFields) {
            $productFields = $resolveInfo->getFieldSelection(4);
            $productFields = $productFields['widgets']['product_cms_page']['products'] ?? [];
        }

        if (!$productFields) {
            $productFields = $resolveInfo->getFieldSelection(5);
            $productFields = $productFields['sections']['widgets']['product_cms_page']['products'] ?? [];
        }
        
        if (!$productFields) {
            return $callable($resolveInfo);
        }

        $sectionNames = ['items', 'product'];

        $fieldNames = [];
        foreach ($sectionNames as $sectionName) {
            if (isset($productFields[$sectionName])) {
                foreach (array_keys($productFields[$sectionName]) as $fieldName) {
                    $fieldNames[] = $this->fieldTranslator->translate($fieldName);
                }
            }
        }

        return array_unique($fieldNames);
    }
}
